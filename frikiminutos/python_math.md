## Python como calculadora

Para tener una calculadora, simplemnte usa el intérprete de Python.

```
python3
>>> 2+2
4
>>> 4*4
16
>>> 1000 / 166.386
6.010121043837823
>>> resultado = (2.34 * 34) / (45 + 45.6)
>>> resultado
0.8781456953642385
>>> (resultado + 23.2) * (2 / 3)
16.052097130242824
>>> quit()
```

Hay muchas operaciones en el paquete estándar math. Por ejemplo (factorial, combinado, mínimo común múltiplo):

```
python3
>>> import math
>>> math.factorial(4)
24
>>> math.comb(4, 3)
4
>>> math.gcd(60, 21, 90)
3
```


Para profundizar:

* [Documentación del módulo math](https://docs.python.org/3/library/math.html)
