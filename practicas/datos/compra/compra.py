#!/usr/bin/env pythoh3

'''
Programa para mostrar la lista de la compra
'''

habitual = ('patatas', 'leche', 'pan')


def main():
    seguir = True
    especifica = []
    while seguir:
        elemento = input("Elemento a comprar: ")
        if elemento == "":
            seguir = False
        else:
            especifica.append(elemento)
    print("Lista de la compra:")
    mostrados = []
    for elemento in list(habitual) + especifica:
        if elemento not in mostrados:
            mostrados.append(elemento)
            print(elemento)
    print("Elementos habituales:", len(habitual))
    print("Elementos específicos:", len(especifica))
    print("Elementos en lista:", len(mostrados))


if __name__ == '__main__':
    main()
