### Practicando con un diccionario

Juega con las distintas acciones que podemso realizar con diccionarios. Utiliza el fichero [diccionario.py](diccionario.py) para:

* Ejecutarlo tal cual
* Ejecutarlo en el depurador, viendo cómo van cambiando sus contenidos, y cómo se extraen y se introducen datos en él
* Modificarlo, completándolo y cambiando, para experimentar con el diccionario
* Repasar cómo funciona `sys.argv` (que es uan lista) y cómo podemos usar sus valores para ponerlos en un diccionario.