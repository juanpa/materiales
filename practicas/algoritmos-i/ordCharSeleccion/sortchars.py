#!/usr/bin/env pythoh3

"""Programa para las letras de una palabra que se recibe como argumento.

Se usa orden de diccionario: una letra va antes o después que otra con
independencia de que sean mayúsculas o minúsculas.

Algoritmo de ordenación por selección.
"""

import sys

def search_minimum(word: str, current_pos : int, lower_pos : int) -> int:
    if word[current_pos].lower() < word[lower_pos].lower():
        lower_pos = current_pos
    return lower_pos
        
def find_lower(word: str, pivot: int) -> int:
    lower: int = pivot
    for pos in range(pivot, len(word)):
        lower = search_minimum(word, pos, lower)
    return lower

def sorted_string(word_chars: list) -> list:
    for pivot_pos in range(len(word_chars)):
        lower_pos: int = find_lower(word_chars, pivot_pos)
        if lower_pos != pivot_pos:
            word_chars[pivot_pos], word_chars[lower_pos] \
                = word_chars[lower_pos], word_chars[pivot_pos]
    
    return word_chars

def main():
    word: str = sys.argv[1]
    word_chars: list = list(word)
    
    sort_str : list = sorted_string(word_chars)
    
    sorted_word = ''.join(sort_str)
    print(sorted_word)

if __name__ == '__main__':
    main()
