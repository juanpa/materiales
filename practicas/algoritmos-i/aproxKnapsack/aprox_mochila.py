"""
  Resuelve el problema de la mochila usando búsqueda exhaustiva.

  Args:
    items: Una lista de objetos, donde cada objeto es una tupla (valor, peso).
    capacity: La capacidad de la mochila.

  Returns:
    La combinación de objetos que maximiza el valor total y que no excede la capacidad de la mochila.
"""
import sys 
import itertools

def opt_knapsack(items, capacity):
    # Inicializar la solución.
    best_solution = []
    best_value = 0
    combinations = []

    for item in range(len(items) + 1):
      for combination in itertools.combinations(items, item):
        total_weight = 0  
        for item in combination:
          weight_of_current_item = item[1]
          total_weight = total_weight + weight_of_current_item
        if total_weight <= capacity:
          combinations.append(combination)
          
    # Evaluar la combinación.
    for combination in combinations:
        value = sum(item[0] for item in combination)
        if value > best_value:
            best_solution = combination
            best_value = value

    return best_solution

def read_arguments():
  if len(sys.argv) < 2:
      sys.exit(f"Usage: {sys.argv[0]} <max_weight> <value1> <weight1> ... <valueN> <weightN>")

  try:
      max_weight = int(sys.argv[1])
      items = []
      for i in range(2, len(sys.argv)-1, 2):
          value = int(sys.argv[i])
          weight = int(sys.argv[i+1])
          items.append((value, weight))
  except ValueError:
      sys.exit("All arguments must be integers")

  return max_weight, items

def main():
  # Ejemplo de uso.
 
  # Read the arguments.
  max_weight, items = read_arguments()

  # Llamar a la función de búsqueda exhaustiva.
  solution = opt_knapsack(items, max_weight)

  # Imprimir la solución.
  print(solution)

if __name__ == "__main__":
  main()
