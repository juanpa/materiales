### Búsqueda de palabras

Realiza un programa, que se llame `searchwords.py`, al que se proporcionará como argumentos en la línea de comandos una palabra y una lista de palabras. El programa escribirá en pantalla la lista de palabras ordenada, y la posición de la palabra en la lista de palabras ordenada (empezando por 0). Por ejemplo:

```commandline
python3 searchwords.py Hola hola adios voy vengo
adios Hola vengo Voy
1
```

o, otro ejemplo:

```commandline
python3 searchwords.py Hola voy hola adios vengo
adios Hola vengo Voy
1
```


La lista de palabras se ordenará sin tener en cuenta mayúsculas o minúsculas (esto es, tanto la `a` como la `A` irán antes de la `b` o la `B`).

El programa debe usar el esquema que muestra el fichero `plantilla.py` que puedes encontrar en el repositorio de plantilla de esta práctica. En él hay varias funciones:

* El programa principal está en la función `main`, como ya hemos hecho en otras prácticas.

* La búsqueda la hace la función `search_word`, que recibe como parámetros una palabra (que es la palabra a buscar) y una lista ordenada de palabras (que es la lista donde se va a buscar la posición de la palabra). Esta función devuelve la posición donde se ha encontrado la palabra en la lista ordenada, o levanta la excepción `Exception` si no la ha encontrado.

Como la lista de palabras que se pasará como argumento al programa puede no estar ordenada, antes de que `main` pueda llamar a `search_word` tendrá que ordenar la lista. Para eso usaremos la función `sort`, del módulo `sortwords` (disponible en el repositorio plantilla). Igualmente, para mostrar la lista de palabras ordenadas, usaremos `show`, también de `sortwords`.

La función `main` tendrá que comprobar también:

* Que se han pasado al programa al menos dos argumentos (además del nombre del programa) en la línea de comandos (que sería una palabra a buscar, y una lista de una palabra donde hacer la búsqueda). Si no es así, debe llamar a `sys.exit` con un mensaje de error adecuado.

* Que no se ha levantado la excepción `Exception` en la función `search_word`. Si se hubiera levantado, tiene que capturarla, y terminar el programa llamando a `sys.exit`.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_searchwords.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_searchwords.py
```

En el repositorio plantilla hay además un fichero `.gitlab-ci.yml`, que hará que el sistema CI (Continuous Integration) de GitLab ejecute automáticamente los tests cada vez que se suba un nuevo commit. Puedes ver el resultado en la opción correspondiente del menú vertical de GitLab (cuando estés viendo tu repositorio).
