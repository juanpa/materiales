### Búsqueda binaria en lista ordenada de números

Realiza un programa que busque la posición (índice) de un número dado en una lista de números que se le proporcionen como argumentos en la línea de comandos. Por ejemplo:

```commandline
python3 search.py 9 4 6 9 13 15 17
2
```

El primer número que el programa recibe como argumento (en este ejemplo, el 9) será el número a buscar. El resto de los números serán la lista donde hay que buscarlo (en este caso, 4, 6, 9, 13, 15, 17. El resultado que muestra el programa es el número de órden (índice, empezando por 0) del número buscado dentro de esta lista.

Para realizar la búsqueda de un número n en una lista l, se utilizará el algortmo básico de búsqueda binaria:

* Comenzamos fijando un pivote izquierdo y un pivote derecho, que se inicializan al índice menor de la lista (`0`) y al índice mayor (`len(l)-1`)).
* Mientras no hayamos encontrado el número buscado:
  * Fijamos un índice de prueba que sea el índice medio entre el pivote izquierdo y el derecho, y vemos cuál es el valor del número en esa posición de la lista (número en índice de prueba).
  * Si el número en el índice de prueba es el buscado, hemos terminaddo, y el valor buscado es el índice de prueba.
  * Si el número en el índice de prueba es mayor que el buscado, el índice del número buscado tiene que estar a su izquierda, así que asignamos al pivote derecho el valor del índice de prueba (no hace falta ya buscar a la derecha del índice de prueba).
  * Si el número en el índice de prueba es menor que el buscado, el índice del número buscado tiene que estar a su derecha, así que asignamos al pivote izquierdo el valor del índice de prueba (no hace falta ya buscar a la izquierda del índice de prueba).


Solución: [search.py](search.py)
