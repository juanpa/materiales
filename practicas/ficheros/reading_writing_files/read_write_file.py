#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Práctica de uso de archivos lectura y escritura.
"""

import sys
import os


def insert_file():
    guests = open("guests.txt", "w")
    initial_guests = ["Bob", "Andrea", "Manuel", "Polly", "Khalid"]
    for i in initial_guests:
        guests.write(i + "\n")
    guests.close()

def add_file():
    new_guests = ["Sam", "Danielle", "Jacob"]

    with open("guests.txt", "a") as guests:
        for i in new_guests:
            guests.write(i + "\n")
    guests.close()

def remove_file():
    checked_out = ["Andrea", "Manuel", "Khalid"]
    temp_list = []
    with open("guests.txt", "r") as guests:
        for g in guests:
            temp_list.append(g.strip())
    with open("guests.txt", "w") as guests:
        for name in temp_list:
            if name not in checked_out:
                guests.write(name + "\n")

def check_file():
    guests_to_check = ['Bob', 'Andrea']
    checked_in = []
    with open("guests.txt", "r") as guests:
        for g in guests:
            checked_in.append(g.strip())
        for check in guests_to_check:
            if check in checked_in:
                print("{} is checked in".format(check))
            else:
                print("{} is not checked in".format(check))
def print_file():
    with open("guests.txt") as guests:
        for line in guests:
            print(line)


def main():
    print("**** Ingreso datos iniciales: ")
    insert_file()
    print_file()
    print("**** Agrego datos:")
    add_file()
    print_file()
    print("***** Remuevo datos:")
    remove_file()
    print_file()
    print("**** Verifico datos:")
    check_file()

    """ if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
     string = sys.argv[1]
    """


if __name__ == '__main__':
    main()
