frase = input("Ingresa una frase: ")
frase_modificada = ""

for caracter in frase:
    if caracter.lower() in "aeiouáéíóú":
        frase_modificada += caracter.upper()
    else:
        frase_modificada += caracter

print("Frase con vocales en mayúsculas:", frase_modificada)
