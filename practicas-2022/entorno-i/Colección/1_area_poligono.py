num_lados = 5
longitud_lado = 8

perimetro = num_lados * longitud_lado
apotema = longitud_lado / (2 * (0.5 * num_lados / 3.14159))
area = (perimetro * apotema) / 2

print("Perímetro:", perimetro)
print("Apotema:", apotema)
print("Área:", area)
