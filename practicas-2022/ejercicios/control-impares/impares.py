#!/usr/bin/env pythoh3

'''
Program to compute sum of odd numbers between two given
'''

import sys

try:
    num1: int = int(input("Dame un número entero no negativo: "))
    num2: int = int(input("Dame otro: "))
except:
    print("Error: comprueba que has escrito un número entero")

if (num1 < 0) or (num2 < 0):
    print("Ambos números han de ser no negativos")
    sys.exit()

if num1 > num2:
    num1, num2 = num2, num1

suma: int = 0
for numero in range(num1, num2+1):
    if (numero % 2) == 1:
        suma = suma + numero

print(suma)