#!/usr/bin/env pythoh3

"""Programa para calcular el factorial"""

import sys

def factorial(n: int):
    if n == 2:
        return 2
    else:
        return n * factorial(n-1)

def main():
    number: int = int(sys.argv[1])
    result = factorial(number)
    print(result)

if __name__ == '__main__':
    main()
