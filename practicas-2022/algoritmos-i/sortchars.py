#!/usr/bin/env pythoh3

"""Programa para las letras de una palabra que se recibe como argumento

Se usa orden de diccionario: una letra va antes o después que otra con
independencia de que sean mayúsculas o minúsculas
"""

import sys

def find_lower(word: str, pivot: int) -> int:
    lower: int = pivot
    for pos in range(pivot, len(word)):
        if word[pos].lower() < word[lower].lower():
            lower = pos
    return lower

def main():
    word: str = sys.argv[1]
    word_chars: list = list(word)
    for pivot_pos in range(len(word_chars)):
        lower_pos: int = find_lower(word_chars, pivot_pos)
        if lower_pos != pivot_pos:
            word_chars[pivot_pos], word_chars[lower_pos] \
                = word_chars[lower_pos], word_chars[pivot_pos]
    word = ''.join(word_chars)
    print(word)

if __name__ == '__main__':
    main()
